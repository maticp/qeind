# Day-3 :
---------

### Topics of Day-3 hands-on session

- DFPT exercises (1a, 1b, 2a, 2b, 3)
- TDDFPT exercises (4, 5, 6, 7)

-----------

**Exercise 1:** Calculation of the phonon frequencies at the Gamma point and of the phonon dispersion of silicon.

   cd example1a/
   cd example1b/
    
**Exercise 2:** Calculation of the phonon frequencies at the Gamma point and of the phonon dispersion of the polar semiconductor AlAs.
    
    cd example2a/
    cd example2b/
    

**Exercise 3:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the turbo_davidson.x code

    cd example5/

