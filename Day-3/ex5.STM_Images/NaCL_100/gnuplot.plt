set terminal eps transparent enhanced color font 'Helvetica, 15'
set out "nacl_-2.5_stm.eps"

set pm3d
set palette grey
#set palette rgb 34,35,36
unset surface
set view 0,0

set lmargin at screen 0.0
set rmargin at screen 1.0
set bmargin at screen 0.0
set tmargin at screen 1.0

unset label
unset colorbox
#set contour
set notics
set notitle
f(z)=z**(0.5)

 splot [:][:][] 'nacl_-2.5_stm.gnu' using 1:2:(f($3)) w l title ""
